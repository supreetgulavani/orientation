# Understanding Git basics in 2 minutes
## What is Git
Git is a version-control software that enables a team to track every step in their project development process.

## Peripherals
Here are some essential concepts that one must know while using Git:

**1. Repository**: A Repository is a collection of all the files that make your project. 

**2. GitLab**: It is the second most popular remote storage solution for repositories.
 
**3. Commit**: A commit can be defined as the change that you make to your project.

**4. Push**: Pushing is syncing commits to Gitlab.

**5. Branch**: Branches exist to sort your work. The main files of your project exist in the default "**master**" branch. You can add multiple branches as well.

**6. Merge**: Merging means bringing together as one. When you want to integrate two branches, you call it merging.

**7. Clone**: Cloning a repository means creating a copy on your local machine.

**8. Fork**: Forking is cloning someone else's repo to make a copy of your own. 

# Command Review

## Installing Git
You have to open your terminal and type in the following command:
**sudo apt-get install git**

## Workflow

1. **Clone**:
The command is: **git clone<_repo link_>**

2. **Create new branch**:
The commands are: 
**git checkout master**
**git checkout -b <_branch name_>**

3. **Commit**:
The command is:
**git commit -sv**

4. **Push**:
The command is:
**git push origin <_branch name_>**
